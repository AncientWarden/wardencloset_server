# Run express server with green lock

### `node ./server.js`

Ref : [https://www.npmjs.com/package/greenlock-express](https://www.npmjs.com/package/greenlock-express)

### Deployment

The application will be dockerize and push the Cloud run which is a serverless platform to scale up and down according to the usage. So we would have less maintenance effort as well as less cost.

#### Deployment Prerequisites

- Install `gcloud CLI` (https://cloud.google.com/sdk/docs/install)
- Setup the account `gcloud auth login`
- Setup the configirations

```sh
gcloud config configurations create warden-closet
gcloud config set account YOUR_EMAIL
gcloud config set project warden-closet
gcloud config set compute/region asia-southeast1
```

#### Deploying

run `npm run deploy`
