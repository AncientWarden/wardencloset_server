const express = require('express')
const app = express()
const path = require('path');
const jimp = require('jimp');
const compression = require('compression')
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser')
const url = require('url');
const cors = require('cors');
const { trueCasePathSync } = require('true-case-path')

const port = process.env.PORT || 5000;
require('bytenode');
app.use(cors());
app.use(fileUpload({
    fileSize: 10 * 1024 * 1024 * 1024 //10MB max file(s) size
}));
app.use(compression())
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use( bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

app.get('/rest/', async  (req, res) => {
    let data = {"status":403,"errMsg": { code: null, hint: "Invalid Parameter"} }
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.send(data)
});

app.get('/v2/', async  (req, res) => {
    let q = url.parse(req.url, true).query;

    if (q && q.id === '3000'){
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.header("Content-Type", "image/*");
        res.sendFile('3000.gif', { root: __dirname });
    }
    else if (q && q.mode !== null && q.mode !== undefined){
        imageOverlay(q, res);
    }
});


app.post('/rest/v2', async function(req, res) {
    // let q = url.parse(req.url, true).query;
    let files = req.files;
    let allUpload = {}

    let q = null
    if(req.body.data) {
        try { q = JSON.parse(req.body.data) } 
        catch(e) { error(res, `failed to parse body data: ${res.body.data}`); }
    }


    if (files && files["upload-body"] && files["upload-body"].length === undefined){
        allUpload["upload-body"] = files["upload-body"].data
    }
    if (files && files["upload-head"] && files["upload-head"].length === undefined){
        allUpload["upload-head"] = files["upload-head"].data
    }
    if (files && files["upload-background"] && files["upload-background"].length === undefined){
        allUpload["upload-background"] = files["upload-background"].data
    }
    if (files && files["upload-accessories"] && files["upload-accessories"].length === undefined){
        allUpload["upload-accessories"] = files["upload-accessories"].data
    }
    imageOverlay(q, res, allUpload);
});

function error (res, errorMessage){
    console.error('error:', errorMessage)
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Content-Type", "image/*");
    res.sendFile('3000.gif', { root: __dirname });
}


async function imageOverlay(q, res, files) {
    // const image = await jimp.read(`./halfImg/oriImg/${q.id}.png`);
    let image = new jimp(1024, 1024, 0x0)
    if (q.mode === 'half') {
        if (q.background) {
            const markPath= getTrueCasePath(`./halfImg/background/${q.background}.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite(await jimp.read(markPath), 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }

        if (files && files["upload-background"]){
            const bodyMark = await jimp.read(files["upload-background"]);
            if (!bodyMark) { error(res, `bodyMark: ${bodyMark} not exists`); return false; }
            image.composite(bodyMark, 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }
    
        if (q.accessories) {
            const markPath= getTrueCasePath(`./halfImg/accessories/${q.accessories}.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite(await jimp.read(markPath), 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }

        if (files && files["upload-accessories"]){
            const bodyMark = await jimp.read(files["upload-accessories"]);
            if (!bodyMark) { error(res, `bodyMark: ${bodyMark} not exists`); return false; }
            image.composite(bodyMark, 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }
    
        if (q.base) {
            const markPath= getTrueCasePath(`./halfImg/base/${q.base}_${q.mode}.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite(await jimp.read(markPath), 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }
    
        if (q.body) {
            const markPath= getTrueCasePath(`./halfImg/body/${q.body}.png`);
            const markPath_re = getTrueCasePath(`./halfImg/body/${q.body}.png`.replace("super","common"));

            if (markPath) { 
                image.composite(await jimp.read(markPath), 0, 0, {
                    mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
                })
            }
            else if (markPath_re){
                image.composite(await jimp.read(markPath_re), 0, 0, {
                    mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
                })
            }
            else{
                error(res, `markPath: ${markPath} not exists`); return false; 
            }
        }

        if (files && files["upload-body"]){
            const bodyMark = await jimp.read(files["upload-body"]);
            if (!bodyMark) { error(res, `bodyMark: ${bodyMark} not exists`); return false; }
            image.composite(bodyMark, 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }
    
        if (q.head && !q.mask) {
            const markPath= getTrueCasePath(`./halfImg/head/${q.head}.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite(await jimp.read(markPath), 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }

        if (files && files["upload-head"]){
            const bodyMark = await jimp.read(files["upload-head"]);
            if (!bodyMark) { error(res, `bodyMark: ${bodyMark} not exists`); return false; }
            image.composite(bodyMark, 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }
    
        if (q.face ) {
            let _shift_x = 0;
            if ( (q.mask && q.base === "Golden") || (q.mask && q.base === "Super") ) { _shift_x = -38; }
            const markPath= getTrueCasePath(`./halfImg/face/${q.face}.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite(await jimp.read(markPath), _shift_x, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }

        if (q.mask) {
            const markPath= getTrueCasePath(`./halfImg/mask/${q.mask}.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite(await jimp.read(markPath), 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })

            let _shift_x = 0;
            let facePathStr = `./halfImg/face/Neutral.png`;
            if (q.base === "Golden") { facePathStr = `./halfImg/face/Neutral Gold.png`; _shift_x = -38; }
            if (q.base === "Super") { facePathStr = `./halfImg/face/Neutral Blue.png`; _shift_x = -38; }
            let facePath = getTrueCasePath(facePathStr);
            if (!facePath) { error(res, `markPath: ${facePath} not exists`); return false; }
            image.composite(await jimp.read(facePath), _shift_x, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }
    
        if (q.left) {
            const markPath= getTrueCasePath(`./halfImg/left/${q.left}.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite(await jimp.read(markPath), 80, 220, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }
    
        if (q.right) {
            const markPath= getTrueCasePath(`./halfImg/right/${q.right}.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite(await jimp.read(markPath), 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }

        if (q.style === 'mosaic') {
            const _mosaicPath = getTrueCasePath(`./moImg/${Math.floor(Math.random() * 100)}.png`);
            if (!_mosaicPath) { error(res, `_mosaicPath: ${_mosaicPath} not exists`); return false; }
            image.composite(await jimp.read(_mosaicPath), 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1.0, opacitySource: 0.3
            })
        }
    }

    else if (q.mode === 'full') {
        const margin = 0;
        const img_x = 1024 + margin;
        const img_y = 1024 + margin;
        image = new jimp(img_x, img_y, 0x0)
        
        if (q.background) {
            const markPath= getTrueCasePath(`./fullImg/background/${q.background}.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite( await jimp.read(markPath) , 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }

        if (files && files["upload-background"]){
            const bodyMark = await jimp.read(files["upload-background"]);
            image.composite(bodyMark, 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }

        if (q.car) {
            const markPath= getTrueCasePath(`./fullImg/car/${q.car}_1.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite(await jimp.read(markPath), 0, margin+7, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }

        if (q.accessories && !q.car) {
            const markPath= getTrueCasePath(`./fullImg/accessories/${q.accessories}.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite( await jimp.read(markPath), 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }

        if (files && files["upload-accessories"] && !q.car){
            const bodyMark = await jimp.read(files["upload-accessories"]);
            image.composite( bodyMark, 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }
    
        if (q.base) {
            const markPath= getTrueCasePath(`./fullImg/base/${q.base}_${q.mode}.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite( await jimp.read(markPath), 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }

        if (q.bottom && !q.car) {
            const markPath= getTrueCasePath(`./fullImg/bottom/${q.bottom}.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite( await jimp.read(markPath), 0, -3, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }

        if (q.shoe && !q.car) {
            const markPath= getTrueCasePath(`./fullImg/shoe/${q.shoe}.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite( await jimp.read(markPath), 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }

        if (q.body) {
            const markPath= getTrueCasePath(`./fullImg/body/${q.body}.png`);
            const markPath_re = getTrueCasePath(`./fullImg/body/${q.body}.png`.replace("super","common"));

            if (markPath) { 
                image.composite( await jimp.read(markPath), 0, 0, {
                    mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
                })
            }
            else if (markPath_re){
                image.composite( await jimp.read(markPath_re), 0, 0, {
                    mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
                })
            }
            else{
                error(res, `markPath: ${markPath} not exists`); return false; 
            }
        }


        if (files && files["upload-body"]){
            // console.log("upload-body")
            const bodyMark = await jimp.read(files["upload-body"]);
            if (!bodyMark) { error(res, `bodyMark: ${bodyMark} not exists`); return false; }
            image.composite(bodyMark, 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }

        if (q.head && !q.mask) {
            const markPath= getTrueCasePath(`./fullImg/head/${q.head}.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite( await jimp.read(markPath), 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }

        if (files && files["upload-head"]){
            const bodyMark = await jimp.read(files["upload-head"]);
            image.composite(bodyMark, 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }

            
        if (q.face) {
            let _shift_x = 0;
            if ( (q.mask && q.base === "Golden") || (q.mask && q.base === "Super") ) { _shift_x = -18; }
            const markPath= getTrueCasePath(`./fullImg/face/${q.face}.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite( await jimp.read(markPath) , _shift_x, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }
        
        if (q.mask) {
            const markPath= getTrueCasePath(`./fullImg/mask/${q.mask}.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite(await jimp.read(markPath), 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })

            let _shift_x = 0;
            let facePathStr = `./fullImg/face/Neutral.png`;
            if (q.base === "Golden") { facePathStr = `./fullImg/face/Neutral Gold.png`; _shift_x = -18; }
            if (q.base === "Super") { facePathStr = `./fullImg/face/Neutral Blue.png`; _shift_x = -18; }
            let facePath = getTrueCasePath(facePathStr);
            if (!facePath) { error(res, `markPath: ${facePath} not exists`); return false; }
            image.composite(await jimp.read(facePath), _shift_x, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }


        if (q.left) {
            const markPath= getTrueCasePath(`./fullImg/left/${q.left}.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite( await jimp.read(markPath) , 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }
    
        if (q.right && !q.car) {
            const markPath= getTrueCasePath(`./fullImg/right/${q.right}.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite( await jimp.read(markPath) , 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }

        if (q.car) {
            const markPath= getTrueCasePath(`./fullImg/car/${q.car}_2.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite( await jimp.read(markPath) , 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }
    
        if (q.style === 'mosaic') {
            const _mosaicPath = getTrueCasePath(`./moImg/${Math.floor(Math.random() * 100)}.png`);
            if (!_mosaicPath) { error(res, `_mosaicPath: ${_mosaicPath} not exists`); return false; }
            image.composite( await jimp.read(_mosaicPath), 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1.0, opacitySource: 0.3
            })
        }
    }

    else if (q.mode === 'back') {
        if (q.background) {
            const markPath= getTrueCasePath(`./halfImg/background/${q.background}.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite(await jimp.read(markPath), 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }
    
        if (q.base) {
            const markPath= getTrueCasePath(`./halfImg/base/${q.base}_${q.mode}.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite(await jimp.read(markPath), 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }

        if (q.back) {
            const markPath= getTrueCasePath(`./halfImg/back/${q.back}.png`);
            if (!markPath) { error(res, `markPath: ${markPath} not exists`); return false; }
            image.composite(await jimp.read(markPath), 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1, opacitySource: 1
            })
        }

        if (q.style === 'mosaic') {
            const _mosaicPath = getTrueCasePath(`./moImg/${Math.floor(Math.random() * 100)}.png`);
            if (!_mosaicPath) { error(res, `_mosaicPath: ${_mosaicPath} not exists`); return false; }
            image.composite(await jimp.read(_mosaicPath), 0, 0, {
                mode: jimp.BLEND_SOURCE_OVER, opacityDest: 1.0, opacitySource: 0.3
            })
        }

    }

    if (files){
        image.getBase64(jimp.AUTO, (err, b64) => {
            // console.log(res)
            let data = {"status": 200, "base64": b64 }
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            res.send(data)
        })
    }
    else{
        image.getBuffer(jimp.MIME_PNG, (err, buffer) => {
            // console.log(buffer)
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            res.header("Content-Type", "image/*");
            // res.header("Content-Disposition", `attachment; filename="_imagename_.png"`);
            res.header("Content-Length", `${buffer.length}`);
            res.send(Buffer.from(buffer, 'binary'));
        });
    }
    
}

// running this app in Docker will make file name path case-sensitive
// and will break the app if not resolving path to the correct case
function getTrueCasePath(filePath) {
    try {
        return trueCasePathSync(path.join(__dirname ,filePath));    
    } catch (error) {
        console.error(`[getTrueCasePath] path not found: ${filePath}`)
        return null
    }
    
}

app.use("/rest/images",express.static(__dirname +'/upload'));
app.use('/myapp', express.static(path.join(__dirname, 'myapp')));
app.get('*', function(req, res) { res.sendFile('index.html', {root: path.join(__dirname, 'myapp')});});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))

// q = `{ "id": "1510", "background": "Cloudy Sky", "accessories": "Axe Blue", "base": "common", "body": "common_body_22", "head": "Armor Hat", "face": "Smile", "style": "grayscale", "bottom": "bottom_10", "shoe": "shoe_1", "left": "left_1", "right": "right_2", "back": "back_1", "mode": "full" }`
// q = JSON.parse(q)
// imageOverlay(q)


module.exports = app;